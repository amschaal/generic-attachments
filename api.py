from rest_framework import viewsets
# from models import 
from serializers import *
from permissions import AttachmentGuardianPermission
from rest_framework import filters
class AttachmentViewSet(viewsets.ModelViewSet):
    filter_fields = ('content_type', 'object_id')
#     filter_backends = (filters.DjangoObjectPermissionsFilter,)
    def pre_save(self, obj):
        """
        Set the object's creator, based on the incoming request.
        """
        print "PRE SAVE"
        print self.request.user
        if not obj.created_by:
            obj.created_by = self.request.user
        obj.modified_by = self.request.user
class FileViewSet(AttachmentViewSet):
    serializer_class = FileSerializer
    permission_classes = [AttachmentGuardianPermission]
#     search_fields = ('name', 'description')
    model = File
    def get_queryset(self):
        return File.objects.all()
    
class NoteViewSet(AttachmentViewSet):
    serializer_class = NoteSerializer
    permission_classes = [AttachmentGuardianPermission]
#     permission_classes = [CustomPermission]
#     filter_fields = ('content_type', 'object_id')
    model = Note
    def get_queryset(self):
        return Note.objects.all()#get_all_user_objects(self.request.user, ['view'], Experiment)

class URLViewSet(AttachmentViewSet):
    permission_classes = [AttachmentGuardianPermission]
    serializer_class = URLSerializer
    model = URL
    def get_queryset(self):
        return URL.objects.all()#get_all_user_objects(self.request.user, ['view'], Experiment)