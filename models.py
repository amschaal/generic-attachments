from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

#
# Attach a file to just about anything:
# file = File(text='My wonderful note',created_by=request.user,content_object=some_model_instance)
# file.save()
class Attachment(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()#models.CharField(max_length=30) #Can be coerced into integer key if necessary
    content_object = GenericForeignKey('content_type', 'object_id')
    created_by = models.ForeignKey(User,null=True,blank=True, related_name='+')
    created = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User,null=True,blank=True, related_name='+')
    modified = models.DateTimeField(auto_now=True)
    admin_only = models.BooleanField(default=True)
    class Meta:
        abstract = True

class File(Attachment):
    file = models.FileField(upload_to='files')
    name = models.CharField(max_length=100)
    description = models.TextField(null=True,blank=True)
    def url(self):
        return reverse('get_file', kwargs={'pk':self.id})
    def __unicode__(self):              # __unicode__ on Python 2
        return self.file.name[:30]+'...'
    class Meta:
        permissions = (
            ('view_file', 'View file'),
        )

class Note(Attachment):
    parent = models.ForeignKey('Note',null=True,blank=True)
    content = models.TextField()
    def __unicode__(self):              # __unicode__ on Python 2
        return self.content[:50]+'...'
    class Meta:
        permissions = (
            ('view_note', 'View note'),
        )
        
class URL(Attachment):
    url = models.URLField()
    text = models.CharField(max_length=250,null=True,blank=True)
    description = models.TextField(null=True,blank=True)
    def __unicode__(self):              # __unicode__ on Python 2
        return str(self.url)
    class Meta:
        permissions = (
            ('view_url', 'View url'),
        )
