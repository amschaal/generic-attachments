import django.dispatch

create_attachment = django.dispatch.Signal(providing_args=["request"])
save_attachment = django.dispatch.Signal(providing_args=["instance","request"])
delete_attachment = django.dispatch.Signal(providing_args=["instance","request"])