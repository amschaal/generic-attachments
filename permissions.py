from rest_framework import permissions
from signals import create_attachment, save_attachment, delete_attachment
    
class AttachmentGuardianPermission(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    def has_permission(self, request, view):
        #@todo: figure out why this gets triggered when doing GET request in REST Framework browser
        if request.method in ['POST']:
            create_attachment.send(getattr(view,'model'),request=request)
        return True
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.method in ['PUT','PATCH']:
            save_attachment.send(obj.__class__,request=request,instance=obj)
        if request.method in ['DELETE']:
            delete_attachment.send(obj.__class__,request=request,instance=obj)
        return True
    
#@deprecated: use AttachmentGuardianPermission to send signals instead
class CustomObjectPermissions(permissions.DjangoObjectPermissions):
    """
    Similar to `DjangoObjectPermissions`, but adding 'view' permissions.
    """
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }
