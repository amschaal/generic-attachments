
angular.module('main')
.controller('FileController', ['$scope','$modal','File', FileController]);

function FileController($scope,$modal,$File) {
	var FileDefaults={};
	$scope.setFileDefaults = function(defaults){
		FileDefaults = defaults;
	};
	$scope.save = function(file){
		var success = function(response){
			console.log('success');
			file.editing = false;
		}
		var error = function(response){
			file.errors = response.data;
		}
		if(file.id)
			file.$save(success,error);
		else
			file.$create(success,error);
	};
	$scope.newFile = function(){
		var file = new $File(FileDefaults);
		$scope.files.push(file);
		$scope.editFile($scope.files.length-1);
	};
	$scope.deleteFile = function(file,index){
		if (!confirm("Are you sure you want to delete this File?"))
			return;
		var id = file.id;
		if (id)
			file.$remove(
					function(){$scope.files.splice(index,1);},
					function(response){
						if (response.data.detail)
							alert(response.data.detail);
					}
					);
		else
			$scope.files.splice(index,1);
	};
	$scope.init = function(){
		$scope.files = $File.query(FileDefaults);
	}
	$scope.editFile = function (index) {
	    var modalInstance = $modal.open({
	      templateUrl: 'editFile.html',
	      controller: 'EditFileController',
	      size: 'large',
	      resolve: {
	        files: function () {
	          return $scope.files;
	        },
	        index: function(){
	        	return index; 
	        }
	      }
	    });

	    modalInstance.result.then(function (index) {
	    }, function () {
	    });
	  };
}

angular.module('main').controller('EditFileController', function ($scope, $modalInstance, files, index) {

	  $scope.files = files;
	  $scope.index = index;
	  $scope.file = angular.copy($scope.files[index]);
	  $scope.save = function(){
			var success = function(response){
				$scope.files[index]= $scope.file;
				$modalInstance.close($scope.index);
			}
			var error = function(response){
				$scope.file.errors = response.data;
			}
			if($scope.file.id)
				$scope.file.$save(success,error);
			else
				$scope.file.$create(success,error);
		};
	  $scope.cancel = function(){
		  if(!$scope.file.id)
			  $scope.files.splice(index,1);
		  $modalInstance.dismiss('cancel');
	  }
	});
