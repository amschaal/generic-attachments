
angular.module('main')
.controller('URLController', ['$scope','$modal','URL', URLController]);

function URLController($scope,$modal,$URL) {
	var URLDefaults={};
	$scope.setURLDefaults = function(defaults){
		URLDefaults = defaults;
	};
	$scope.newURL = function(){
		var url = new $URL(URLDefaults);
		$scope.urls.push(url);
		$scope.editURL($scope.urls.length-1);
	};
	$scope.deleteURL = function(url,index){
		if (!confirm("Are you sure you want to delete this URL?"))
			return;
		var id = url.id;
		if (id)
			url.$remove(
					function(){$scope.urls.splice(index,1);},
					function(response){
						if (response.data.detail)
							alert(response.data.detail);
					}
			);
		else
			$scope.urls.splice(index,1);
	};
	$scope.init = function(){
		$scope.urls = $URL.query(URLDefaults);
	}
	$scope.editURL = function (index) {
	    var modalInstance = $modal.open({
	      templateUrl: 'editURL.html',
	      controller: 'EditURLController',
	      size: 'large',
	      resolve: {
	        urls: function () {
	          return $scope.urls;
	        },
	        index: function(){
	        	return index; 
	        }
	      }
	    });

	    modalInstance.result.then(function (index) {
	    }, function () {
	    });
	  };
}

angular.module('main').controller('EditURLController', function ($scope, $modalInstance, urls, index) {

	  $scope.urls = urls;
	  $scope.index = index;
	  $scope.url = angular.copy($scope.urls[index]);
	  $scope.save = function(){
			var success = function(response){
				$scope.urls[index]= $scope.url;
				$modalInstance.close($scope.index);
			}
			var error = function(response){
				$scope.url.errors = response.data;
			}
			if($scope.url.id)
				$scope.url.$save(success,error);
			else
				$scope.url.$create(success,error);
		};
	  $scope.cancel = function(){
		  if(!$scope.url.id)
			  $scope.urls.splice(index,1);
		  $modalInstance.dismiss('cancel');
	  }
	});
