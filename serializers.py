from rest_framework import serializers
from models import File, Note, URL
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name')

class AttachmentSerializer(serializers.ModelSerializer):
    created_by = UserSerializer(read_only=True)
    modified_by = UserSerializer(read_only=True)
        
class FileSerializer(AttachmentSerializer):
    url = serializers.Field(source='url')
    class Meta:
        model = File
        
class NoteSerializer(AttachmentSerializer):
    class Meta:
        model = Note

class URLSerializer(AttachmentSerializer):
    class Meta:
        model = URL